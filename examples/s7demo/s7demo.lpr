////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
//  Example for using PASETTIMINO and S7EXTENDED and direct communication to Siemens Simatic S7 PLC family    //
//                                                                                                            //
//  Original Settimino for Arduino converted to Free Pascal by Zeljko Avramovic (user avra in Lazarus forum)  //
//                                                                                                            //
//  Original library documentation and license can be found at http://settimino.sourceforge.net               //
//  This source is published under the same license as original library (commercial use is allowed).          //
//  If you need more powerfull heavy weight library then you can take a look at http://snap7.sourceforge.net  //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
// - This Pasettimino and S7Extended demo uses ConnectTo() with Rack=0 and Slot=3 (S7400)                     //
//   - If you want to connect to S7300 then change them to Rack=0 and Slot=2.                                 //
//   - If you want to connect to S71200/S71500 change them to Rack=0, Slot=0.                                 //
//   - If you want to connect to S7400 see your hardware configuration.                                       //
//   - If you want to work with a LOGO 0BA7 or S7200 please refer to the                                      //
//     documentation and change                                                                               //
//     Client.ConnectTo(<IP>, <Rack>, <Slot>);                                                                //
//     with the couple                                                                                        //
//     Client.SetConnectionParams(<IP>, <LocalTSAP>, <Remote TSAP>);                                          //
//     Client.Connect();                                                                                      //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

program s7demo;

{$mode delphi}{$H+}

{$define aaa}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  udemo;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TS7TestForm, S7TestForm);
  Application.Run;
end.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
//  PASETTIMINO - S7PAS - Ultra light library for direct communication to Siemens Simatic S7 PLC family.      //
//                                                                                                            //
//  Compatible with S7 200/300/400/400H/1200/1500 and LOGO 0BA7.                                              //
//                                                                                                            //
//  Converted to Free Pascal by Zeljko Avramovic (user avra in Lazarus forum)                                 //
//                                                                                                            //
//  Original library documentation and license can be found at http://settimino.sourceforge.net               //
//  This source is published under the same license as original library (commercial use is allowed).          //
//  If you need more powerfull heavy weight library then you can take a look at http://snap7.sourceforge.net  //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unit s7pas;

{$mode objfpc}

interface

uses
  SysUtils, Classes,
  blcksock, synautil;

{$define _EXTENDED}    // settimino memory model: (_SMALL, _NORMAL, _EXTENDED)

{$ifdef _NORMAL}
  {$define _S7HELPER}
{$endif}

{$ifdef _EXTENDED}
  {$define _S7HELPER}
{$endif}

{$define PDU480}

const
  // Error Codes
  // from 0x0001 up to 0x00FF are severe errors, the Client should be disconnected
  // from 0x0100 are S7 Errors such as DB not found or address beyond the limit etc..
  // For Arduino Due the error code is a 32 bit integer but this doesn't change the constants use.
  errTCPConnectionFailed = $0001;
  errTCPConnectionReset  = $0002;
  errTCPDataRecvTout     = $0003;
  errTCPDataSend         = $0004;
  errTCPDataRecv         = $0005;
  errISOConnectionFailed = $0006;
  errISONegotiatingPDU   = $0007;
  errISOInvalidPDU       = $0008;
  //
  errS7InvalidPDU        = $0100;
  errS7SendingPDU        = $0200;
  errS7DataRead          = $0300;
  errS7DataWrite         = $0400;
  errS7Function          = $0500;
  //
  errBufferTooSmall      = $0600;
  //
  ErrNone = 0; // the only missing defined error code in synapse and settimino libraries

  // Connection Type
  PG       = $01;
  OP       = $02;
  S7_Basic = $03;

  // ISO and PDU related constants
  ISOSize    =   7;  // Size of TPKT + COTP Header
  isotcp     = 102;  // ISOTCP Port
  MinPduSize =  16;  // Minimum S7 valid telegram size
  {$ifdef PDU480}
  MaxPduSize = 487;  // Maximum S7 valid telegram size (we negotiate 480 bytes + ISOSize)
  {$else}
  MaxPduSize = 247;  // Maximum S7 valid telegram size (we negotiate 240 bytes + ISOSize)
  {$endif}
  CC         = $D0;  // Connection confirm
  Shift      =  17;  // We receive data 17 bytes above to align with PDU.DATA[]

  // S7 ID Area (Area that we want to read/write). There may be several aliases for the same area.
  S7AreaPE = $81;   S7AreaI = S7AreaPE;   S7AreaPI = S7AreaPE;
  S7AreaPA = $82;   S7AreaO = S7AreaPA;   S7AreaPQ = S7AreaPA;   S7AreaQ = S7AreaPA;
  S7AreaMK = $83;   S7AreaM = S7AreaMK;
  S7AreaDB = $84;   S7AreaD = S7AreaDB;
  S7AreaCT = $1C;   S7AreaC = S7AreaCT;
  S7AreaTM = $1D;   S7AreaT = S7AreaTM;

  // WordLength
  S7WLBit     = $01;
  S7WLByte    = $02;
  S7WLWord    = $04;
  S7WLDWord   = $06;
  S7WLReal    = $08;
  S7WLCounter = $1C;
  S7WLTimer   = $1D;

  S7CpuStatusUnknown                 = $00; // Extended. Settimino used only statuses $00, $04 and $08
  S7CpuStatusStop_Update             = $01;
  S7CpuStatusStop_ResetMemory        = $02;
  S7CpuStatusStop_SelfInitialization = $03;
  S7CpuStatusStop                    = $04;
  S7CpuStatusStartup_ColdRestart     = $05;
  S7CpuStatusStartup_WarmRestart     = $06;
  S7CpuStatusStartup_HotRestart      = $07;
  S7CpuStatusRun                     = $08;
  S7CpuStatusRun_Redundant           = $09; // This can only be returned with SZL $0071 for S7-400H cpu when in redundancy
  S7CpuStatusHold                    = $0A;
  S7CpuStatusLinkUp                  = $0B;
  S7CpuStatusUpdate                  = $0C;
  S7CpuStatusDefective               = $0D;
  S7CpuStatusSelfTest                = $0E;
  S7CpuStatusNoPower                 = $0F;

  RxOffset = 18;
  Size_RD  = 31;
  Size_WR  = 35;

  // For further informations about structures (the byte arrays and they meanins)
  // see http://snap7.sourceforge.net project.
  // Arduino has not a multithread environment and all Client functions are
  // fully synchronous, so to save memory we can define telegrams and I/O
  // data areas as globals, since only one client at time will use them.

var
  // ISO Connection Request telegram (contains also ISO Header and COTP Header)
  ISO_CR: array[0..21] of byte = (
    // TPKT (RFC1006 Header)
    $03, // RFC 1006 ID (3)
    $00, // Reserved, always 0
    $00, // High part of packet lenght (entire frame, payload and TPDU included)
    $16, // Low part of packet lenght (entire frame, payload and TPDU included)
    // COTP (ISO 8073 Header)
    $11, // PDU Size Length
    $E0, // CR - Connection Request ID
    $00, // Dst Reference HI
    $00, // Dst Reference LO
    $00, // Src Reference HI
    $01, // Src Reference LO
    $00, // Class + Options Flags
    $C0, // PDU Max Length ID
    $01, // PDU Max Length HI
    $0A, // PDU Max Length LO
    $C1, // Src TSAP Identifier
    $02, // Src TSAP Length (2 bytes)
    $01, // Src TSAP HI (will be overwritten by ISOConnect())
    $00, // Src TSAP LO (will be overwritten by ISOConnect())
    $C2, // Dst TSAP Identifier
    $02, // Dst TSAP Length (2 bytes)
    $01, // Dst TSAP HI (will be overwritten by ISOConnect())
    $02  // Dst TSAP LO (will be overwritten by ISOConnect())
  );

  // S7 PDU Negotiation Telegram (contains also ISO Header and COTP Header)
  S7_PN: array[0..24] of byte = (
    $03, $00, $00, $19, $02, $f0, $80, // TPKT + COTP (see above for info)
    $32, $01, $00, $00, $04, $00, $00, $08, $00,
    $00, $f0, $00, $00, $01, $00, $01,
    {$ifdef PDU480}
    $01, $e0 // PDU Length Requested = HI-LO 480 bytes
    {$else}
    $00, $f0 // PDU Length Requested = HI-LO 240 bytes
    {$endif}
  );

  // S7 Read/Write Request Header (contains also ISO Header and COTP Header)
  S7_RW: array [0..34] of byte = ( // 31-35 bytes
    $03, $00,
    $00, $1f,       // Telegram Length (Data Size + 31 or 35)
    $02, $f0, $80,  // COTP (see above for info)
    $32,            // S7 Protocol ID
    $01,            // Job Type
    $00, $00,       // Redundancy identification
    $05, $00,       // PDU Reference
    $00, $0e,       // Parameters Length
    $00, $00,       // Data Length = Size(bytes) + 4
    $04,            // Function 4 Read Var, 5 Write Var
    $01,            // Items count
    $12,            // Var spec.
    $0a,            // Length of remaining bytes
    $10,            // Syntax ID
    S7WLByte,       // Transport Size
    $00, $00,       // Num Elements
    $00, $00,       // DB Number (if any, else 0)
    $84,            // Area Type
    $00, $00, $00,  // Area Offset
    // WR area
    $00,            // Reserved
    $04,            // Transport size
    $00, $00        // Data Length * 8 (if not timer or counter)
  );

{$ifdef _EXTENDED}

  // S7 Get Block Info Request Header (contains also ISO Header and COTP Header)
  S7_BI: array [0..36] of byte = (
    $03, $00, $00, $25, $02, $f0, $80, $32,
    $07, $00, $00, $05, $00, $00, $08, $00,
    $0c, $00, $01, $12, $04, $11, $43, $03,
    $00, $ff, $09, $00, $08, $30, $41,
    $30, $30, $30, $30, $30, // ASCII DB Number
    $41
  );

  // S7 Put PLC in STOP state Request Header (contains also ISO Header and COTP Header)
  S7_STOP: array[0..32] of byte = (
    $03, $00, $00, $21, $02, $f0, $80, $32,
    $01, $00, $00, $0e, $00, $00, $10, $00,
    $00, $29, $00, $00, $00, $00, $00, $09,
    $50, $5f, $50, $52, $4f, $47, $52, $41,
    $4d
  );

  // S7 Put PLC in RUN state Request Header (contains also ISO Header and COTP Header)
  S7_START: array [0..36] of byte = (
    $03, $00, $00, $25, $02, $f0, $80, $32,
    $01, $00, $00, $0f, $00, $00, $14, $00,
    $00, $28, $00, $00, $00, $00, $00, $00,
    $fd, $00, $00, $09, $50, $5f, $50, $52,
    $4f, $47, $52, $41, $4d
  );

  // S7 Get PLC Status Request Header (contains also ISO Header and COTP Header)
  S7_PLCGETS: array [0..32] of byte = (
    $03, $00, $00, $21, $02, $f0, $80, $32,
    $07, $00, $00, $2c, $00, $00, $08, $00,
    $08, $00, $01, $12, $04, $11, $44, $01,
    $00, $ff, $09, $00, $04, $04, $24, $00,
    $00
  );

{$endif} // _EXTENDED

const
  MAX_BIT_NUMBER   = 7;
  MAX_BUFFER_INDEX = MaxPduSize + Shift - 1;           // 263
  MAX_HEADER_INDEX = Size_WR - 1;                      // 34
  MAX_DATA_INDEX   = MAX_BUFFER_INDEX - Size_WR;       // 228

type
  //word     = uint16;        // 16 bit unsigned integer
  //integer  = int16;         // 16 bit signed integer
  int      = int16;           // 16 bit signed integer
  dword    = longword;        // 32 bit unsigned integer
  dint     = longint;         // 32 bit signed integer
  float    = single;          // 32 bit single precision floating point number

  pbyte    = ^byte;
  pword    = ^word;
  pdword   = ^dword;
  pint     = ^int;
  pdint    = ^dint;
  pfloat   = ^single;

  TBitNumber = byte;                // should be 0..MAX_BIT_NUMBER;
  TIndexPDU  = 0..MAX_BUFFER_INDEX; // word

  TPDU = packed record
    H:    array[0..MAX_HEADER_INDEX] of byte; // PDU Header
    DATA: array[0..MAX_DATA_INDEX] of byte;   // PDU Data
  end;

  TPDURAW = packed record
    // Bytes: array[TIndexPDU] of byte; // 0..MaxPduSize+Shift-1 this was for AVR, on PC we use functions with bigger memory usage so 4096 was chosen
    Bytes: array[0..4095] of byte;
  end;

  TIPBytes = array [0..3] of byte;

  TIPAddress = packed record
    case Integer of
      0: (Bytes: TIPBytes);
      1: (DoubleWord: dword);
  end;

  { TS7Client }

  TS7Client = class
  private
    FConnectTimeout: Integer;
    FLocalTSAP_HI: uint8;
    FLocalTSAP_LO: uint8;
    FRemoteTSAP_HI: uint8;
    FRemoteTSAP_LO: uint8;
    FLastPDUType: uint8;
    FConnType: uint16;
    FPeer: TIPAddress;
    FPDULength: int;    // PDU Length negotiated
    function  IsoPduSize: int;
    function  WaitForData(aSize: uint16; aTimeout: uint16): int;
    function  RecvPacket(aBufAddr: pbyte; aSize: uint16): int;
    function  TCPConnect: int;
    function  ISOConnect: int;
    function  NegotiatePduLength: int;
    procedure SetConnectTimeout(AValue: Integer);
  protected
    TCPClient: TTCPBlockSocket;
    function SetLastError(aError: int): int;
    function RecvISOPacket(var aSize: word): int;
  public
    // Output properties
    Connected: boolean;   // true if the Client is connected
    LastError: int;       // Last Operation error
    // Input properties
    RecvTimeout: uint16; // Receving timeout
    // Methods
    constructor Create;
    destructor  Destroy; override;
    // Basic functions
    procedure SetConnectionParams(aAddress: TIPAddress; aLocalTSAP: uint16; aRemoteTSAP: uint16);
    procedure SetConnectionType(aConnectionType: uint16);
    function  ConnectTo(aAddress: TIPAddress; aRack: uint16; aSlot: uint16): int;
    function  ConnectTo(aIP3, aIP2, aIP1, aIP0: byte; aRack: uint16; aSlot: uint16): int;
    function  Connect: int;
    procedure Disconnect;
    function  ReadArea(aArea: int; aDBNumber: uint16; aStart: uint16; aAmount: uint16; aPtrData: pointer): int;
    function  WriteArea(aArea: int; aDBNumber: uint16; aStart: uint16; aAmount: uint16; aPtrData: pointer): int;
    function  GetPDULength: int;
    // Extended functions
  {$ifdef _EXTENDED}
    function  GetDBSize(aDBNumber: uint16): word; overload;
    function  GetDBSize(aDBNumber: uint16; var aSize: word): int; overload;
    function  DBGet(aDBNumber: uint16; aPtrData: pointer; var aSize: word): int;
    function  PlcStart: int; // Warm start
    function  PlcStop: int;
    function  GetPlcStatus(var aCpuStatus: int): int; virtual;
    function  IsoExchangeBuffer(var aSize: word): int;
    function  ErrorToString(aError: int): string; overload;
    function  CpuStatusToString(const aCpuStatus: int): string;
    // My extensions
    function  ReadBoolean(Area: int; aDBNumber: uint16; aStart: uint16; aBitNumber: byte): boolean;
    function  ReadByte(Area: int; aDBNumber: uint16; aStart: uint16): uint8;
    function  ReadWord(Area: int; aDBNumber: uint16; aStart: uint16): uint16;
    function  ReadInteger(aArea: int; aDBNumber: uint16; aStart: uint16): int;
    function  ReadLongWord(aArea: int; aDBNumber: uint16; aStart: uint16): uint32;
    function  ReadLongInt(aArea: int; aDBNumber: uint16; aStart: uint16): int32;
    function  ReadFloat(aArea: int; aDBNumber: uint16; aStart: uint16): float;
  {$endif}
    property ConnectTimeout: Integer read FConnectTimeout write SetConnectTimeout; // Timeout for connect
  end;

  function IPBytesToIPString(IP: TIPAddress): string;

{$ifdef _S7HELPER}
  function GetBit(aValue: byte; aIndex: Byte): boolean;
  function BitAt(aBuffer: pointer; aByteIndex: TIndexPDU; aBitIndex: TBitNumber): boolean; overload;
  function BitAt(aByteIndex: TIndexPDU; aBitIndex: TBitNumber): boolean; overload;
  function ByteAt(aBuffer: pointer; aIndex: TIndexPDU): uint8; overload;
  function ByteAt(aIndex: TIndexPDU): uint8; overload;
  function WordAt(aBuffer: pointer; aIndex: TIndexPDU): uint16; overload;
  function WordAt(aIndex: TIndexPDU): uint16; overload;
  function DwordAt(aBuffer: pointer; aIndex: TIndexPDU): uint32; overload;
  function DwordAt(aIndex: TIndexPDU): uint32; overload;
  function FloatAt(aBuffer: pointer; aIndex: TIndexPDU): float; overload;
  function FloatAt(aIndex: TIndexPDU): float; overload;
  function IntegerAt(aBuffer: pointer; aIndex: TIndexPDU): int16; overload;
  function IntegerAt(aIndex: TIndexPDU): int16; overload;
  function DintAt(aBuffer: pointer; aIndex: TIndexPDU): int32; overload;
  function DintAt(aIndex: TIndexPDU): int32; overload;
{$endif} // _S7HELPER


var
  PDURAW: TPDURAW;
  PDU: TPDU absolute PDURAW;

implementation

function IPBytesToIPString(IP: TIPAddress): string;
begin
  Result := IntToStr(IP.Bytes[0]) + '.' + IntToStr(IP.Bytes[1]) + '.' + IntToStr(IP.Bytes[2]) + '.' + IntToStr(IP.Bytes[3]);
end;

//-----------------------------------------------------------------------------
{$ifdef _S7HELPER}

function GetBit(aValue: byte; aIndex: Byte): boolean;
begin
  Result := ((aValue shr aIndex) and 1) = 1;
end;

function BitAt(aBuffer: pointer; aByteIndex: TIndexPDU; aBitIndex: TBitNumber): boolean;
var
  BytePointer: pbyte;
begin
  Result := false;
  if aBitIndex <= MAX_BIT_NUMBER then
  begin
    BytePointer := pbyte(aBuffer) + aByteIndex;
    Result := GetBit(BytePointer^, aBitIndex);
  end;
end;

function BitAt(aByteIndex: TIndexPDU; aBitIndex: TBitNumber): boolean;
begin
  Result := BitAt(pbyte(PDU.DATA), aByteIndex, aBitIndex);
end;

function ByteAt(aBuffer: pointer; aIndex: TIndexPDU): uint8;
var
  BytePointer: pbyte;
begin
  BytePointer := pbyte(aBuffer) + aIndex;
  Result := BytePointer^;
end;

function ByteAt(aIndex: TIndexPDU): uint8;
begin
  Result := ByteAt(pbyte(PDU.DATA), aIndex);
end;

function WordAt(aBuffer: pointer; aIndex: TIndexPDU): uint16;
begin
  Result := ByteAt(aBuffer, aIndex);
  Result := (Result shl 8) + ByteAt(aBuffer, aIndex + 1);
end;

function WordAt(aIndex: TIndexPDU): uint16;
begin
  Result := WordAt(pbyte(PDU.DATA), aIndex);
end;

function DwordAt(aBuffer: pointer; aIndex: TIndexPDU): uint32;
begin
  Result := ByteAt(aBuffer, aIndex);
  Result := (Result shl 8) + ByteAt(aBuffer, aIndex + 1);
  Result := (Result shl 8) + ByteAt(aBuffer, aIndex + 2);
  Result := (Result shl 8) + ByteAt(aBuffer, aIndex + 3);
end;

function DwordAt(aIndex: TIndexPDU): uint32;
begin
  Result := DWordAt(pbyte(PDU.DATA), aIndex);
end;

function FloatAt(aBuffer: pointer; aIndex: TIndexPDU): float;
var
  lw: longword;
  f: float absolute lw; // overlay variable
begin
  // Result := single(DWordAt(aBuffer, aIndex)); // this typecasting did not work
  lw := DWordAt(aBuffer, aIndex);
  Result := f;
end;

function FloatAt(aIndex: TIndexPDU): float;
begin
  Result := FloatAt(pbyte(PDU.DATA), aIndex);
end;

function IntegerAt(aBuffer: pointer; aIndex: TIndexPDU): int16;
begin
  Result := int16(WordAt(aBuffer, aIndex));
end;

function IntegerAt(aIndex: TIndexPDU): int16;
begin
  Result := IntegerAt(pbyte(PDU.DATA), aIndex);
end;

function DintAt(aBuffer: pointer; aIndex: TIndexPDU): int32;
begin
  Result := dint(DWordAt(aBuffer, aIndex));
end;

function DintAt(aIndex: TIndexPDU): int32;
begin
  Result := DIntAt(pbyte(PDU.DATA), aIndex);
end;

{$endif}  // _S7HELPER
//-----------------------------------------------------------------------------



//------------------------------------------------------------------------
constructor TS7Client.Create;
begin
  inherited Create;
  TCPClient := TTCPBlockSocket.Create;
  // Default TSAP values for connectiong as PG to a S7300 (Rack 0, Slot 2)
  FLocalTSAP_HI   := $01;
  FLocalTSAP_LO   := $00;
  FRemoteTSAP_HI  := $01;
  FRemoteTSAP_LO  := $02;
  FConnType       := PG;
  Connected       := false;
  LastError       := 0;
  FPDULength      := 0;
  RecvTimeout     := 500; // 500 ms
  FConnectTimeout := 1000;
  TCPClient.ConnectionTimeout := FConnectTimeout; // forward connection timeout to Synapse
end;

destructor TS7Client.Destroy;
begin
  Disconnect;
  TCPClient.Free;
  inherited Destroy;
end;

function TS7Client.GetPDULength: int;
begin
  Result := FPDULength;
end;

function TS7Client.SetLastError(aError: int): int;
begin
  LastError := aError;
  Result    := aError;
end;

function TS7Client.WaitForData(aSize: uint16; aTimeout: uint16): int;
var
  Elapsed: QWord;
  BytesReady: uint16;
begin
  Elapsed := GetTickCount64;

  repeat
    // I don't like next function because imho is buggy, it returns 0 also if _sock==MAX_SOCK_NUM
    // but it belongs to the standard Ethernet library and there are too many dependencies to skip them.
    // So be very carefully with the munber of the clients, they must be <=4.
    BytesReady := TCPClient.WaitingData;
    if BytesReady < aSize then
      Sleep(1)
    else
    begin
      Result := SetLastError(0);
      Exit;
    end;

    // Check for rollover - should happen every 52 days without turning off Arduino.
    if (GetTickCount64 < Elapsed) then
      Elapsed := GetTickCount64; // Resets the counter, in the worst case we will wait some additional millisecs.
  until not (GetTickCount64 - Elapsed < aTimeout);

  // Here we are in aTimeout zone, if there's something into the buffer, it must be discarded.
  if BytesReady > 0 then
    TCPClient.Purge
  else
    if not Connected then // originaly here was TCPClient.Connected
    begin
      Result := SetLastError(errTCPConnectionReset);
      Exit;
    end;

  Result := SetLastError(errTCPDataRecvTout);
end;

function TS7Client.IsoPduSize: int16;
var
  Size: uint16;
begin
  Size := PDU.H[2];
  Result := (Size shl 8) + PDU.H[3];
end;

procedure TS7Client.SetConnectTimeout(AValue: Integer);
begin
  if FConnectTimeout = AValue then
    Exit;
  FConnectTimeout := AValue;
  TCPClient.ConnectionTimeout := FConnectTimeout;
end;

function TS7Client.RecvPacket(aBufAddr: pbyte; aSize: uint16): int16;
begin
  WaitForData(aSize, RecvTimeout);
  if LastError <> 0 then
  begin
    Result := LastError;
    Exit;
  end;
  if TCPClient.RecvBuffer(aBufAddr, aSize) = 0 then
  begin
    Result := SetLastError(errTCPConnectionReset);
    Exit;
  end;
  Result := SetLastError(0);
end;

procedure TS7Client.SetConnectionParams(aAddress: TIPAddress; aLocalTSAP: uint16; aRemoteTSAP: uint16);
begin
  FPeer.DoubleWord := aAddress.DoubleWord;
  FLocalTSAP_HI    := aLocalTSAP  shr 8;
  FLocalTSAP_LO    := aLocalTSAP  and $00FF;
  FRemoteTSAP_HI   := aRemoteTSAP shr 8;
  FRemoteTSAP_LO   := aRemoteTSAP and $00FF;
end;

procedure TS7Client.SetConnectionType(aConnectionType: uint16);
begin
  FConnType := aConnectionType;
end;

function TS7Client.ConnectTo(aAddress: TIPAddress; aRack: uint16; aSlot: uint16): int;
begin
  SetConnectionParams(aAddress, $0100, word(FConnType shl 8) + (aRack * $20) + aSlot);
  Result := Connect;
end;

function TS7Client.ConnectTo(aIP3, aIP2, aIP1, aIP0: byte; aRack: uint16; aSlot: uint16): int;
var // IP address example: 192.168.5.11
  Address: TIPAddress;
begin
  Address.Bytes[0] := aIP3; // 192
  Address.Bytes[1] := aIP2; // 168
  Address.Bytes[2] := aIP1; //   5
  Address.Bytes[3] := aIP0; //  11
  Result := ConnectTo(Address, aRack, aSlot);
end;

function TS7Client.Connect: int16;
begin
  LastError := 0;
  if not Connected then
  begin
    TCPConnect;
    if LastError = 0 then  // First stage : TCP Connection
    begin
      ISOConnect;
      if LastError = 0 then // Second stage : ISOTCP (ISO 8073) then  Connection
        LastError := NegotiatePduLength; // Third stage : S7 PDU negotiation
    end;
  end;
  Connected := LastError = 0;
  Result := LastError;
end;

procedure TS7Client.Disconnect;
begin
  if Connected then
  begin
    TCPClient.CloseSocket;
    Connected  := false;
    FPDULength := 0;
    LastError  := 0;
  end;
end;

function TS7Client.TCPConnect: int16;
begin
  TCPClient.Connect(IPBytesToIPString(FPeer), IntToStr(isotcp));
  if TCPClient.LastError = ErrNone then
    Result := SetLastError(0)
  else
    Result := SetLastError(errTCPConnectionFailed);
 end;

function TS7Client.RecvISOPacket(var aSize: word): int16;
var
  Done: boolean;
  Target: pbyte;
begin
  Done      := false;
  Target    := pbyte(@PDU.H[0])+Shift;
  LastError := 0;

  while (LastError = 0) and (not Done) do
  begin
    // Get TPKT (4 bytes)
    RecvPacket(PDU.H, 4);
    if LastError = 0 then
    begin
      aSize := IsoPduSize;
      // Check 0 bytes Data Packet (only TPKT+COTP - 7 bytes)
      if aSize = 7 then
        RecvPacket(PDU.H, 3) // Skip remaining 3 bytes and Done is still false
      else
      begin
        if ((aSize > MaxPduSize) or (aSize < MinPduSize)) then
          LastError := errISOInvalidPDU
        else
          Done := true; // a valid Length !=7 && >16 && <247
       end;
     end;
   end;
  if LastError = 0 then
  begin
    RecvPacket(PDU.H, 3); // Skip remaining 3 COTP bytes
    FLastPDUType := PDU.H[1]; // Stores PDU aType, we need it
    aSize := aSize - ISOSize;
    // We need to align with PDU.DATA
    RecvPacket(Target, aSize);
   end;
  if LastError <> 0 then
    TCPClient.Purge;
  Result := LastError;
end;

function TS7Client.ISOConnect: int16;
var
  Length: uint16 = 0;
begin
  // Setup TSAPs
  ISO_CR[16] := FLocalTSAP_HI;
  ISO_CR[17] := FLocalTSAP_LO;
  ISO_CR[20] := FRemoteTSAP_HI;
  ISO_CR[21] := FRemoteTSAP_LO;

  if TCPClient.SendBuffer(@ISO_CR, SizeOf(ISO_CR)) = SizeOf(ISO_CR) then
  begin
    RecvISOPacket(Length);
    if (LastError = 0) and (Length = 15) then // 15 = 22 (sizeof CC telegram) - 7 (sizeof Header) then
    begin
      if FLastPDUType = CC then  // Connection confirm
        Result := 0
      else
        Result:= SetLastError(errISOInvalidPDU);
    end
    else
      Result := LastError;
  end
  else
    Result := SetLastError(errISOConnectionFailed);
end;

function TS7Client.NegotiatePduLength: int16;
var
  Length: uint16 = 0;
begin
  if TCPClient.SendBuffer(@S7_PN, SizeOf(S7_PN)) = SizeOf(S7_PN) then
  begin
    RecvISOPacket(Length);
    if LastError = 0 then
    begin // check S7 Error
      if ((Length = 20) and (PDU.H[27] = 0) and (PDU.H[28] = 0)) then   // 20 = size of Negotiate Answer
      begin
        FPDULength := PDURAW.Bytes[35]; // FPDULength := PDU.H[35];
        FPDULength := (FPDULength shl 8) + PDURAW.Bytes[36]; // Value negotiated // FPDULength := (FPDULength shl 8) + PDU.H[36];
        if FPDULength > 0 then
          Result := 0
        else
          Result := SetLastError(errISONegotiatingPDU);
      end
      else
        Result := SetLastError(errISONegotiatingPDU);
    end
    else
      Result := LastError;
  end
  else
    Result := SetLastError(errISONegotiatingPDU);
end;

function TS7Client.ReadArea(aArea: int16; aDBNumber: uint16; aStart: uint16; aAmount: uint16; aPtrData: Pointer): int16;
var
  Address: longword;
  NumElements, MaxElements, TotElements, SizeRequested: uint16;
  Length: uint16 = 0;
  Target: pbyte;
  Offset: uintptr;
  WordSize: int16;
begin
  Offset    := 0;
  WordSize  := 1;
  LastError := 0;

  // If we are addressing Timers or counters the element size is 2
  if ((aArea = S7AreaCT) or (aArea = S7AreaTM)) then
    WordSize := 2;

  MaxElements := (FPDULength-18) div WordSize; // 18 = Reply telegram header
  TotElements := aAmount;
  // If we use the internal buffer only, we cannot exced the PDU limit
  if aPtrData = nil then
    if TotElements > MaxElements then
      TotElements := MaxElements;

  while (TotElements > 0) and (LastError = 0) do
  begin
    NumElements := TotElements;
    if NumElements > MaxElements then
      NumElements := MaxElements;

    SizeRequested := NumElements * WordSize;

    Target := pbyte(aPtrData) + Offset;

    // Setup the telegram
    Move(S7_RW, PDU.H, Size_RD);

    // Set DB Number
    PDU.H[27] := aArea;
    if aArea = S7AreaDB then
    begin
      PDU.H[25] := aDBNumber shr 8;
      PDU.H[26] := aDBNumber and $00FF;
    end;

    // Adjusts aStart and word length
    if (aArea = S7AreaCT) or (aArea = S7AreaTM) then
    begin
      Address := aStart;
      if aArea = S7AreaCT then
        PDU.H[22] := S7WLCounter
      else
        PDU.H[22] := S7WLTimer;
    end
    else
      Address := aStart shl 3;

    // Num elements
    PDU.H[23] := NumElements shr 8;     // PDU.H[23] := NumElements shl 8; (original before bugfix by Avra)
    PDU.H[24] := NumElements and $00FF; // PDU.H[24] := NumElements;       (original before bugfix by Avra)

    // Address into the PLC
    PDU.H[30] := Address and $000000FF;
    Address   := Address shr 8;
    PDU.H[29] := Address and $000000FF;
    Address   := Address shr 8;
    PDU.H[28] := Address and $000000FF;

    if TCPClient.SendBuffer(@PDU.H, Size_RD) = Size_RD then
    begin
      RecvISOPacket(Length);
      if LastError = 0 then
      begin
        if Length >= 18 then
        begin
          if (Length - 18 = SizeRequested) and (PDU.H[31] = $FF) then
          begin
            if aPtrData <> nil then // original: if aPtrData <> 0 then
              Move(PDU.DATA[0], Target^, SizeRequested); // Copies in the user's buffer
            Offset := Offset + SizeRequested;
          end
          else
            LastError := errS7DataRead;
        end
        else
          LastError := errS7InvalidPDU;
       end;
    end
    else
      LastError := errTCPDataSend;

    TotElements := TotElements - NumElements;
    aStart := aStart + NumElements * WordSize;
  end;
  Result := LastError;
end;

function TS7Client.WriteArea(aArea: int16; aDBNumber: uint16; aStart: uint16; aAmount: uint16; aPtrData: Pointer): int16;
var
  Address: longword;
  NumElements, MaxElements, TotElements, DataSize, IsoSize, Length: uint16;
  Source: pbyte;
  Offset: uintptr = 0;
  WordSize: int16;
begin
  LastError := 0;

  // If we are addressing Timers or counters the element size is 2
  if (aArea = S7AreaCT) or (aArea = S7AreaTM) then
    WordSize := 2
  else
    WordSize := 1; // added by Avra (it was missing)

  MaxElements := (FPDULength - 35) div WordSize; // 35 = Write telegram header
  TotElements := aAmount;
  if aPtrData = nil then // original: if aPtrData = 0 then
    if TotElements > MaxElements then
      TotElements := MaxElements;

  while (TotElements > 0) and (LastError = 0) do
  begin
    NumElements := TotElements;
    if NumElements > MaxElements then
      NumElements := MaxElements;
    // If we use the internal buffer only, we cannot exced the PDU limit
    DataSize := NumElements * WordSize;
    IsoSize  := Size_WR + DataSize;

    // Setup the telegram
    Move(S7_RW, PDU.H, Size_WR);
    // Whole telegram Size
    PDU.H[2] := IsoSize shr 8;
    PDU.H[3] := IsoSize and $00FF;
    // Data Length
    Length := DataSize+4;
    PDU.H[15] := Length shr 8;
    PDU.H[16] := Length and $00FF;
    // Function
    PDU.H[17] := $05;
    // Set DB Number
    PDU.H[27] := aArea;
    if aArea = S7AreaDB then
    begin
      PDU.H[25] := aDBNumber shr 8;
      PDU.H[26] := aDBNumber and $00FF;
    end;
    // Adjusts aStart and word length
    if (aArea = S7AreaCT) or (aArea = S7AreaTM) then
    begin
      Address := aStart;
      Length  := DataSize;
      if aArea = S7AreaCT then
        PDU.H[22] := S7WLCounter
      else
        PDU.H[22] := S7WLTimer;
    end
    else
    begin
      Address := aStart shl 3;
      Length  := DataSize shl 3;
    end;
    // Num elements
    PDU.H[23] := NumElements shr 8;       // fix by Avra, original: PDU.H[23] := NumElements shl 8
    PDU.H[24] := NumElements and $00FF; ; // fix by Avra, original: PDU.H[24] := NumElements;
    // Address into the PLC
    PDU.H[30] := Address and $000000FF;
    Address   := Address shr 8;
    PDU.H[29] := Address and $000000FF;
    Address   := Address shr 8;
    PDU.H[28] := Address and $000000FF;
    // Length
    PDU.H[33] := Length shr 8;
    PDU.H[34] := Length and $00FF;
    // Copy data
    Source := pbyte(aPtrData) + Offset;
    if aPtrData <> nil then
      Move(Source^, PDURAW.Bytes[35], DataSize); // Move(@PDU.H[35], Source, DataSize);

    if TCPClient.SendBuffer(@PDU.H, IsoSize) = IsoSize then
    begin
      RecvISOPacket(Length);
      if LastError = 0 then
      begin
        if Length = 15 then
        begin
          if (PDU.H[27] <> $00) or (PDU.H[28] <> $00) or (PDU.H[31] <> $FF) then
            LastError := errS7DataWrite;
        end
        else
          LastError := errS7InvalidPDU;
      end;
    end
    else
      LastError := errTCPDataSend;

    Offset := Offset + DataSize;
    TotElements := TotElements - NumElements;
    aStart := aStart + NumElements * WordSize;
  end;
  Result := LastError;
end;

//-----------------------------------------------------------------------------
{$ifdef _EXTENDED}

function TS7Client.GetDBSize(aDBNumber: uint16): word;
var
  Size: word = 0;
begin
  if GetDBSize(aDBNumber, Size) = ErrNone then
    Result := Size
  else
    Result := 0;
end;

function TS7Client.GetDBSize(aDBNumber: uint16; var aSize: word): int16;
var
  Length: uint16 = 0;
begin
  LastError := 0;
  aSize := 0;
  // Setup the telegram
  Move(S7_BI, PDU.H, SizeOf(S7_BI));
  // Set DB Number
  PDU.H[31] := (aDBNumber div 10000) + $30;
  aDBNumber :=  aDBNumber mod 10000;
  PDU.H[32] := (aDBNumber div 1000) + $30;
  aDBNumber :=  aDBNumber mod 1000;
  PDU.H[33] := (aDBNumber div 100) + $30;
  aDBNumber :=  aDBNumber mod 100;
  PDU.H[34] := (aDBNumber div 10) + $30;
  aDBNumber :=  aDBNumber mod 10;
  PDURAW.Bytes[35] := (aDBNumber div 1) + $30; // PDU.H[35] := (aDBNumber div 1) + $30;
  if TCPClient.SendBuffer(@PDU.H, SizeOf(S7_BI)) = SizeOf(S7_BI) then
  begin
    RecvISOPacket(Length);
    if LastError = 0 then
    begin
      if Length > 25 then  // 26 is the minimum expected
      begin
        if (PDURAW.Bytes[37] = $00) and (PDURAW.Bytes[38] = $00) and (PDURAW.Bytes[39] = $FF) then // if (PDU.H[37] = $00) and (PDU.H[38] = $00) and (PDU.H[39] = $FF) then
        begin
          aSize := PDURAW.Bytes[83]; // aSize^ := PDU.H[83];
          aSize := (aSize shl 8) + PDURAW.Bytes[84]; //aSize^ := (aSize^ shl 8) + PDU.H[84];
        end
        else
          LastError := errS7Function;
      end
      else
        LastError := errS7InvalidPDU;
    end;
  end
  else
    LastError := errTCPDataSend;

  Result := LastError;
end;

function TS7Client.DBGet(aDBNumber: uint16; aPtrData: Pointer; var aSize: word): int16;
var
  Length: uint16 = 0;
begin
  Result := GetDBSize(aDBNumber, Length);
  if Result = 0 then
  begin
    if Length <= aSize then  // Check if the buffer supplied is big enough
    begin
      Result := ReadArea(S7AreaDB, aDBNumber, 0, Length, aPtrData);
      if Result = 0 then
        aSize := Length;
    end
    else
      Result := errBufferTooSmall;
  end;
end;

function TS7Client.PlcStop: int16;
var // this is actualy a CPU stop and not PLC stop in a redundant S7-400H PLC
  Length: uint16 = 0;
begin
  LastError := 0;
  // Setup the telegram
  Move(S7_STOP, PDU.H, SizeOf(S7_STOP));
  if TCPClient.SendBuffer(@PDU.H, SizeOf(S7_STOP)) = SizeOf(S7_STOP) then
  begin
    RecvISOPacket(Length);
    if LastError = 0 then
    begin
      if Length > 12 then  // 13 is the minimum expected
      begin
        if (PDU.H[27] <> $00) or (PDU.H[28] <> $00) then
          LastError := errS7Function;
      end
      else
        LastError := errS7InvalidPDU;
    end;
  end
  else
    LastError := errTCPDataSend;
  Result:= LastError;
end;

function TS7Client.PlcStart: int16;
var // this is actualy a CPU start and not PLC start in a redundant S7-400H PLC
  Length: uint16 = 0;
begin
  LastError := 0;
  // Setup the telegram
  Move(S7_START, PDU.H, SizeOf(S7_START));
  if TCPClient.SendBuffer(@PDU.H, SizeOf(S7_START)) = SizeOf(S7_START) then
  begin
    RecvISOPacket(Length);
    if LastError = 0 then
    begin
      if Length > 12 then  // 13 is the minimum expected
      begin
        if (PDU.H[27] <> $00) or (PDU.H[28] <> $00) then
          LastError := errS7Function;
      end
      else
        LastError := errS7InvalidPDU;
    end;
  end
  else
    LastError := errTCPDataSend;
  Result:= LastError;
end;

function TS7Client.GetPlcStatus(var aCpuStatus: int): int;
var // this is actualy a CPU status and not PLC status in a redundant S7-400H PLC
  Length: uint16 = 0;
begin
  LastError := 0;
  Move(S7_PLCGETS, PDU.H, SizeOf(S7_PLCGETS)); // setup the telegram
  if TCPClient.SendBuffer(@PDU.H, SizeOf(S7_PLCGETS)) = SizeOf(S7_PLCGETS) then
  begin
    RecvISOPacket(Length);
    if LastError = 0 then
    begin
      if Length > 53 then  // 54 is the minimum expected
      begin
        if (PDURAW.Bytes[54]) in [S7CpuStatusStop_Update..S7CpuStatusNoPower] then // status $01..$0F
        begin // fixed Settimino bug which returned S7CpuStatusStop instead of S7CpuStatusRun_Redundant on S7-400H
          aCpuStatus := PDURAW.Bytes[54]; // aCpuStatus^ := PDU.H[54];
          if aCpuStatus = S7CpuStatusStop_SelfInitialization then
            aCpuStatus := S7CpuStatusStop; // fix for old CPUs when STOP status can be coded as $03
        end
        else
          aCpuStatus := S7CpuStatusUnknown; // status $00
      end
      else
        LastError := errS7InvalidPDU;
    end;
  end
  else
    LastError := errTCPDataSend;
  Result := LastError;
end;

function TS7Client.IsoExchangeBuffer(var aSize: word): int16;
begin
  LastError := 0;

  if TCPClient.SendBuffer(@PDU.H, aSize) = aSize then
    RecvISOPacket(aSize)
  else
    LastError := errTCPDataSend;
  Result := LastError;
end;

function TS7Client.ErrorToString(aError: int): string;
begin
  case aError of
    errTCPConnectionFailed:     Result := 'errTCPConnectionFailed';
    errTCPConnectionReset:      Result := 'errTCPConnectionReset';
    errTCPDataRecvTout:         Result := 'errTCPDataRecvTout';
    errTCPDataSend:             Result := 'errTCPDataSend';
    errTCPDataRecv:             Result := 'errTCPDataRecv';
    errISOConnectionFailed:     Result := 'errISOConnectionFailed';
    errISONegotiatingPDU:       Result := 'errISONegotiatingPDU';
    errISOInvalidPDU:           Result := 'errISOInvalidPDU';

    errS7InvalidPDU:            Result := 'errS7InvalidPDU';
    errS7SendingPDU:            Result := 'errS7SendingPDU';
    errS7DataRead:              Result := 'errS7DataRead';
    errS7DataWrite:             Result := 'errS7DataWrite';
    errS7Function:              Result := 'errS7Function';

    errBufferTooSmall:          Result := 'errBufferTooSmall';
  else
    Result := 'errUnknown';
  end;
end;

function TS7Client.CpuStatusToString(const aCpuStatus: int): string;
begin
  case aCpuStatus of
    S7CpuStatusUnknown:                       Result := 'S7CpuStatusUnknown';
    S7CpuStatusStop_Update:                   Result := 'S7CpuStatusStop_Update';
    S7CpuStatusStop_ResetMemory:              Result := 'S7CpuStatusStop_ResetMemory';
    S7CpuStatusStop_SelfInitialization:       Result := 'S7CpuStatusStop_SelfInitialization';
    S7CpuStatusStop:                          Result := 'S7CpuStatusStop';
    S7CpuStatusStartup_ColdRestart:           Result := 'S7CpuStatusStartup_ColdRestart';
    S7CpuStatusStartup_WarmRestart:           Result := 'S7CpuStatusStartup_WarmRestart';
    S7CpuStatusStartup_HotRestart:            Result := 'S7CpuStatusStartup_HotRestart';
    S7CpuStatusRun:                           Result := 'S7CpuStatusRun';
    S7CpuStatusRun_Redundant:                 Result := 'S7CpuStatusRun_Redundant';
    S7CpuStatusHold:                          Result := 'S7CpuStatusHold';
    S7CpuStatusLinkUp:                        Result := 'S7CpuStatusLinkUp';
    S7CpuStatusUpdate:                        Result := 'S7CpuStatusUpdate';
    S7CpuStatusDefective:                     Result := 'S7CpuStatusDefective';
    S7CpuStatusSelfTest:                      Result := 'S7CpuStatusSelfTest';
    S7CpuStatusNoPower:                       Result := 'S7CpuStatusNoPower';
  else
    Result := 'S7 CPU STATUS NOT RECOGNIZED !';
  end;
end;

// My extensions

function TS7Client.ReadBoolean(Area: int; aDBNumber: uint16; aStart: uint16; aBitNumber: byte): boolean;
begin
  ReadArea(Area, aDBNumber, aStart, 1, nil);
  Result := BitAt(0, aBitNumber);
end;

function TS7Client.ReadByte(Area: int; aDBNumber: uint16; aStart: uint16): uint8;
begin
  ReadArea(Area, aDBNumber, aStart, 1, nil);
  Result := ByteAt(0);
end;

function TS7Client.ReadWord(Area: int; aDBNumber: uint16; aStart: uint16): uint16;
begin
  ReadArea(Area, aDBNumber, aStart, 2, nil);
  Result := WordAt(0);
end;

function TS7Client.ReadInteger(aArea: int; aDBNumber: uint16; aStart: uint16): int;
begin
  ReadArea(aArea, aDBNumber, aStart, 2, nil);
  Result := IntegerAt(0);
end;

function TS7Client.ReadLongWord(aArea: int; aDBNumber: uint16; aStart: uint16): uint32;
begin
  ReadArea(aArea, aDBNumber, aStart, 4, nil);
  Result := DWordAt(0);
end;

function TS7Client.ReadLongInt(aArea: int; aDBNumber: uint16; aStart: uint16): int32;
begin
  ReadArea(aArea, aDBNumber, aStart, 4, nil);
  Result := DIntAt(0);
end;

function TS7Client.ReadFloat(aArea: int; aDBNumber: uint16; aStart: uint16): float;
begin
  ReadArea(aArea, aDBNumber, aStart, 4, nil);
  Result := FloatAt(0);
end;

{$endif}  // _EXTENDED
//-----------------------------------------------------------------------------

end.
